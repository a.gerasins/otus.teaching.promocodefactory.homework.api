﻿using System;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Subscriptions;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Mutation
{
    [ExtendObjectType(Name = "Mutation")]
    public class CustomerMutations
    {
        public async Task<CustomerResponse> CreateCustomer(
            CreateOrEditCustomerRequest input,
            [Service] IRepository<Customer> customerRepository,
            [Service] IRepository<Preference> preferenceRepository
        )
        {
            var preferences = await preferenceRepository
                .GetRangeByIdsAsync(input.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(input, preferences);
            
            await customerRepository.AddAsync(customer);

            return new CustomerResponse(customer);
        }

        public async Task<CustomerResponse> EditCustomer(
            Guid id,
            CreateOrEditCustomerRequest input,
            [Service] IRepository<Customer> customerRepository,
            [Service] IRepository<Preference> preferenceRepository,
            [Service] IRepository<CustomerPreference> customerPreferenceRepository,
            [Service] ITopicEventSender eventSender)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
                return null;
            
            var preferences = await preferenceRepository.GetRangeByIdsAsync(input.PreferenceIds);
            
            await customerPreferenceRepository.DeleteRangeAsync(
                customer.Preferences);

            CustomerMapper.MapFromModel(input, preferences, customer);

            await customerRepository.UpdateAsync(customer);

            await eventSender.SendAsync(customer.Id, customer);
            
            return new CustomerResponse(customer);
        }

        public async Task<bool?> DeleteCustomer(Guid id,
            [Service] IRepository<Customer> customerRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
                return false;

            await customerRepository.DeleteAsync(customer);

            return true;
        }
    }

}